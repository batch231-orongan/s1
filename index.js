console.log("Hello World!");

// Global Objects
// ARrays

let students = [
"Tony",
"Peter",
"Wanda",
"Vision",
"Loki"
];

console.log(students);

// what array method can we use to add an item at the end of the array?

students.push("Thor");
console.log(students);

// What array method can we use to add an item at the start of the array?

students.unshift("Steve");
console.log(students);

// What array method does the opposite of push method?

students.pop();
console.log(students);

// What array method does the opposite of inshift method?

students.shift();
console.log(students);

// What is the difference betweeen splice() and Slice()?
	//.splice() - removes and adds items from a starting index(Mutator Method).
	// .slice() - copies a portion from starting index and returns new array from it(Non-Mutator methods).

// Another kind of array methods?
	// Iteration Methods - loops over the items of an array

// forEach() - loops over items in an array and repeats a user-defined function

// map() - loops over items in an array and repeats a user-defined function AND returns a new array.

// every() - loops and checks if all items satisfy a given condition AND returns a boolean value.

let arrNum =[15, 20, 25, 30, 11];

// check if every item in arrNum is divisible by 5.

let allDivisible;

arrNum.forEach(num => {
	if(num % 5 == 0){
		console.log(`${num} is divisible by 5`)
	}
	else{
		allDivisible = false;
	}
	// However, vcan forEach() return data that woll tell us if all numbers/items in our arrNum is divisible by 5?
});

console.log(allDivisible);

arrNum.pop();
arrNum.push(35);

console.log(arrNum);

let divisibleBy5 = arrNum.every(num =>{
	console.log(num);
	return num % 5 === 0;
})
console.log(divisibleBy5);
// result: true

// MATH
// mathematical constants case sensetive property
// 8 pre-defines properties which can be called via the syntax Math.property 
console.log(Math);
console.log(Math.E); // Euler's Number
console.log(Math.PI); // Pi
console.log(Math.SQRT2); // square root of 2
console.log(Math.SQRT1_2); // square root of 1/2
console.log(Math.LN2); // natural logarithm of 2
console.log(Math.LN10); // natural logarithm of 10
console.log(Math.LOG2E); // base 2 logarithm of E
console.log(Math.LOG10E); // base 10 logarithm of E

// Methods for rounding a number to an integer.
console.log(Math.round(Math.PI)); // It rounds to a nearest interger. : result to 3
console.log(Math.ceil(Math.PI)); // rounds UP to the nearest integer. : result to 4
console.log(Math.floor(Math.PI)); // rounds DOWN to the nearest integer. : result to 3
console.log(Math.trunc(Math.PI)); // returns only the integer part (ES6 updates). : result to 3

// method for returning the square root of a number
console.log(Math.sqrt(3.14)); // result: 1.77

// lowest value in a list of arguments: result to -4
console.log(Math.min(-4, -3, -2, -1, 0, 1, 2, 3, 4));

// highest value in a list of arguments.: result to 4
console.log(Math.max(-4, -3, -2, -1, 0, 1, 2, 3, 4));

//JS PRIMITIVES (no methods, value and properties)
	// String
	// number
	// boolean
	// null
	// undefined

// Activity Solutions
// 1. Create a function named addToEnd that will add a passed in string to the end of a passed array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.

function addToEnd(arr, name){
	if(typeof name == "string"){
		arr.push(name);
		console.log(arr);
	}else{
		console.log("error - can only add strings to an array")
	}
};

//addToEnd(students, "Ryan")
//addToEnd(students, 45)


// 2. Create a function named addToStart that will add a passed in string to the start of a passed array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.

function addToStart(arr, name){
	if(typeof name == "string"){
		arr.unshift(name);
		console.log(arr);
	}else{
		console.log("error - can only add strings to an array")
	}
};

//addToStart(students, "Tess")
//addToStart(students, 33)

// 3. Create a function named elementChecker that will check a passed in array if atleast one of its elements has the same value passed in arguments. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Tony" as arguments when testing.

function elementChecker(arr, name){
	if(arr.includes(name)){
		console.log(true);
	}else{
		console.log("error - passed in array is empty")
	}
};

//elementChecker(students, "Tony")
//elementChecker(students, 33)

// 4. Create a function named checkAllStringsEnding that will accept the passed  array and a character. The function will do the ff:

/*
    if array is empty, return "error - array must NOT be empty"

    if at least one array element is NOT a string, return console.log("error - array must NOT be empty");

    if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"

    if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"

    if every element in the array ends in the passed character, return true. Otherwise return false.

Use the students array and the character "e" as arguments when testing.

*/

function CheckAllStringsEnding(arr, char){
	if(arr.length == 0 || arr == undefined){		
		console.log("error - array must NOT be empty");
	}
	// if(arr.every(typeof arr[])){
	// 	console.log(console.log("error - array must NOT be empty");
	// }
	else if(typeof char !== "string"){
		console.log("error - 2nd argument must be of data type string")
	}else if(char.length > 1){
		console.log("error - 2nd argument must be a single character")
	// }else if(arr.every(arr[arr.length-1] == char)){
	// 	console.log(true);
	}
	else{
		console.log(false);
	}

};

// 5. Create a function name stringLengthSorter that will take in an array of strings as its arguments and sort its elements in an ascending order based on their lenghts. If atleast one element is not a string, return "error- all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.

function stringLengthSorter(arr){
	
	if(arr => arr.every(i => typeof i === "string")){
		arr.sort((a,b) => a.length - b.length);
		console.log(arr);

	}else{
		console.log("error- all array elements must be strings")
	}
};